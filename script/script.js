function openBar() {
    document.getElementById("mySidebar").style.width = "390px";
    document.getElementById("sidebarClose").style.display = "block";
}
  
function closeBar() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("sidebarClose").style.display = "none";
}


window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 115 || document.documentElement.scrollTop > 115) {
        document.getElementById("header").style.backgroundColor = "#fff";
        document.getElementById("header").style.opacity = "0.9";
        let navLink = document.getElementsByClassName("nav__link");
        var i;
        for (i = 0; i < navLink.length; i++) {
          navLink[i].style.color = "#424242";
        }
        let social = document.getElementsByClassName("social__logo");
        var i;
        for (i = 0; i < social.length; i++) {
          social[i].style.opacity = "0";
        }

        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("header").style.backgroundColor = "";
        let x = document.getElementsByClassName("nav__link");
        var i;
        for (i = 0; i < x.length; i++) {
          x[i].style.color = "#fff";
        }
        let social = document.getElementsByClassName("social__logo");
        var i;
        for (i = 0; i < social.length; i++) {
          social[i].style.opacity = "1";
        }
        document.getElementById("myBtn").style.display = "none";
    }
}

$(function(){
  $(window).scroll(function(){
      if($(this).scrollTop()>115){
          $(".logo img").attr("src","images/logo2.png");
      }

      else{
          $(".logo img").attr("src","images/logo.png");
      }
  })
})

var scrollEventHandler = function(){
    window.scroll(0, window.pageYOffset)
}

window.addEventListener("scroll", scrollEventHandler, false);

$(document).ready(function() {
    $(".Slider").slick({
      prevArrow:$('.button__arrow-left'),
      nextArrow:$('.button__arrow-right'),
     
    });
  });

  $(document).ready(function() {
    $(".review-slider").slick({
      dots: true,
      prevArrow:$('.button__arrow-left'),
      nextArrow:$('<button class="button__arrow-right"><img class="arrow-right" src="images/right-arrow.png"></button>')
    });
  });
 


  let x = setInterval(function() {

  let currentDate = new Date().getTime();
  let targetDate = new Date("Sep 13, 2019 10:00:00").getTime();

  let distance = targetDate - currentDate;
  
  let days = Math.floor(distance / (1000 * 60 * 60 * 24));
  let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  let seconds = Math.floor((distance % (1000 * 60)) / 1000);
  
  document.getElementById("day").innerHTML = days;
  document.getElementById("hour").innerHTML = hours;
  document.getElementById("min").innerHTML = minutes;
  document.getElementById("sec").innerHTML = seconds;
  
  
  }, 1000);

function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}